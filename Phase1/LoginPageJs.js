function enter() {
    document.getElementById('id01').style.display = 'block';
}

function closeWindow() {
    document.getElementById('id01').style.display = 'none';
    document.getElementById('id02').style.display = 'none';
}

function register() {
    document.getElementById('id02').style.display = 'block';
}
function loadDoc() {
    console.log("in f");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var obj = JSON.parse(this.responseText);
            $('input[name="name"]').attr("value", obj.Firstname);
            $('input[name="lastname"]').attr("value", obj.Lastname);
            $('input[name="email"]').attr("value", obj.Email);
            $('input[name="student-ID"]').attr("value", obj.STID);
            $('input[name="ID"]').attr("value", obj.NationalCode);
        }
    };
    xhttp.open("GET", "/Phase1/api/editprofile.json", true);
    xhttp.send();
}

(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        let forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        let validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();
